// 預設開啟網頁時的圖號為1
var carouselIndex = 1;

// 開啟網頁馬上執行，讓圖號1為 display:block，其餘 display:none
slide(carouselIndex);

// 按左右按鈕做上、下一張圖片切換
document.querySelector(".prev").onclick = function () {
    slide(carouselIndex -= 1); // 按左鍵時，圖號-1

    console.log(carouselIndex);


    // 增加滑動效果(按左鍵時圖片往左滑)
    var carousels = document.querySelectorAll(".carousels");
    carousels[carouselIndex - 1].className = "carousels left-slide";
}

document.querySelector(".next").onclick = function () {

    slide(carouselIndex += 1); // 按右鍵時，圖號+1

    console.log(carouselIndex);

    // 增加滑動效果(按右鍵時圖片往右滑)
    var carousels = document.querySelectorAll(".carousels");
    carousels[carouselIndex - 1].className = "carousels right-slide";
}


// 選圖片下方豆豆直接跳轉到指定圖號
function assign(n) {
    slide(carouselIndex = n);
}

function slide(n) {

    var i;
    var carousels = document.querySelectorAll(".carousels");
    var dots = document.querySelectorAll(".dot");

    console.log(carousels.length)


    if (n > carousels.length) { carouselIndex = 1 } // 圖號變成4時 ＝ 播第1張圖
    if (n < 1) { carouselIndex = carousels.length } // 圖號變成0時 ＝ 播第3張圖


    // ---new---
    for (i = 0; i < carousels.length; i++) {
        carousels[i].style.display = "none"; // 先把所有圖片設定 display:none
        dots[i].className = dots[i].className.replace(" active", ""); // 先把所有圖片的 active 屬性清空

        if (i == carouselIndex - 1) {
            carousels[i].style.display = "block"; // 僅將選定的圖號加上 display:block
            dots[i].className += " active"; // 僅將選定的圖號加上 active 屬性
        }
    }

    // ---old---
    // for (i = 0; i < carousels.length; i++) {
    //     carousels[i].style.display = "none"; // 先把所有圖片設定 display:none
    // }
    // for (i = 0; i < dots.length; i++) {
    //     dots[i].className = dots[i].className.replace(" active", ""); // 先把所有圖片的 active 屬性清空
    // }

    // carousels[carouselIndex - 1].style.display = "block"; // 僅將選定的圖號加上 display:block
    // dots[carouselIndex - 1].className += " active"; // 僅將選定的圖號加上 active 屬性

    function adddot() {
        var inner = "";
        for (var i = 1; i <= carousels.length; i++) {
            text = `<a class="dot">${i}</a>`
            inner += text;
        }
        return inner;
    }
    
    // console.log(adddot())
    
    document.querySelector(".dot-group").innerHTML = adddot();
    
    // var dotnum = document.querySelectorAll(".dot");
    // console.log(dotnum)
    
    
    // for (var i = 0; i < dotnum.length; i++) {
    //     dotnum[i].num = i;
    
    //     dotnum[i].onclick = function () {
    //         var jumppp = this.num * -imgwidth
    //         console.log(jumppp)
    //         document.querySelector(".test").style.transform = `translateX(${jumppp}px)`;
    //         // console.log(i)
    //     }
    // }
    
    // console.log(dotnum)
}

// var carousels = document.querySelectorAll(".carousels");
// if (n == 1) {
//     carousels[carouselIndex - 1].className = "carousels right-slide";
// } else {
//     carousels[carouselIndex - 1].className = "carousels left-slide";
// }
